#pragma once

#include <iostream>
#include <string>
#include <map>
#include <memory>

#include "Transaction.h"
#include "Money.h"
#include "CreditCard.h"

void CreditCard::printTransactions()
{
	for (std::map<std::string, std::shared_ptr<Transaction>>::iterator it = transactions.begin(); it != transactions.end(); ++it)
	{
		std::cout << *it->second << std::endl;
	}
}

void CreditCard::chargeMethod(std::string itemName, std::shared_ptr<Money> pCost)
{
	this->transactions[itemName] = std::shared_ptr<Transaction>(new Transaction(itemName, pCost));
}

void CreditCard::chargeMethod(std::string itemName, int pEuros, int pCentimes)
{
	this->transactions[itemName] = std::shared_ptr<Transaction>(new Transaction(itemName, std::shared_ptr<Money>(new Money(pEuros, pCentimes))));
}