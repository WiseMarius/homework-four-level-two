#pragma once

#include <iostream>
#include <string>

class GenerateInput
{
public:
	static std::string generateRandomString();
};