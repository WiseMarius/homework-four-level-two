#include "GenerateInput.h"

std::string GenerateInput::generateRandomString()
{
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	std::string transactionName;

	for (int index = 0; index < 10; index++)
	{
		transactionName += alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	return transactionName;
}
