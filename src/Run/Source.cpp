#include <iostream>
#include <time.h>

#include "CreditCard.h"
#include "Money.h"
#include "Transaction.h"
#include "GenerateInput.h"

void populateList(CreditCard *myCreditCard)
{
	std::string tempString;
	for (int index = 0; index < rand() % 10 + 1; index++)
	{
		tempString = GenerateInput::generateRandomString();
		std::cout << "Generated using method #1 " << std::endl;
		myCreditCard->chargeMethod(tempString, std::shared_ptr<Money>(new Money(rand() % 1000 + 1, rand() % 100 + 1)));

		tempString = GenerateInput::generateRandomString();
		std::cout << "Generated using method #2 " << std::endl;
		myCreditCard->chargeMethod(tempString, rand() % 1000 + 1, rand() % 100 + 1);
	}

	std::cout << std::endl;
}

void main()
{
	srand(time(0));

	CreditCard *myCreditCard = new CreditCard();
	
	populateList(myCreditCard);

	myCreditCard->printTransactions();
}