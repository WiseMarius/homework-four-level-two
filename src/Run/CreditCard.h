#pragma once

#include <iostream>
#include <string>
#include <map>
#include <memory>

#include "Transaction.h"
#include"Money.h"

class CreditCard
{
private:
	std::string ownerName;
	std::string cardNumber;
	std::map<std::string, std::shared_ptr<Transaction>> transactions;

public:
	void printTransactions();

	void chargeMethod(std::string itemName, std::shared_ptr<Money> pCost);
	void chargeMethod(std::string itemName, int pEuros, int pCentimes);
};