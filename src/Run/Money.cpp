#include "Money.h"

Money Money::checkMoney(const Money *pMoney)
{
	Money *tempMoney = new Money(*pMoney);

	while (tempMoney->centimes >= 100)
	{
		tempMoney->centimes -= 100;
		tempMoney->euros += 1;
	}

	while (tempMoney->centimes < 0)
	{
		tempMoney->centimes += 100;
		tempMoney->euros -= 1;
	}

	return *tempMoney;
}

Money::Money()
{
	this->euros = 0;
	this->centimes = 0;
}

Money::Money(int pEuros, int pCentimes)
{
	this->euros = pEuros;
	this->centimes = pCentimes;

	if (pCentimes < 10 && pCentimes != 0)
		centimes *= 10;
}

Money::Money(const Money &pMoney)
{
	this->euros = pMoney.euros;
	this->centimes = pMoney.centimes;
}

//Creating getters and setters
int Money::getEuros()
{
	return this->euros;
}
void Money::setEuros(int pEuros)
{
	this->euros = pEuros;
}
int Money::getCentimes()
{
	return this->centimes;
}
void Money::setCentimes(int pCentimes)
{
	this->centimes = pCentimes;
}

//Overloading Inut/Output operators
std::ostream &operator<<(std::ostream &output, const Money &pMoney)
{
	output << pMoney.euros << " euros and " << pMoney.centimes << " cents";

	return output;
}

std::istream &operator >> (std::istream  &input, Money &pMoney)
{
	std::cout << "Value for euros: ";
	input >> pMoney.euros;
	std::cout << "Value for centimes: ";
	input >> pMoney.centimes;

	if (pMoney.centimes < 10 && pMoney.centimes != 0)
	{
		pMoney.centimes *= 10;
	}

	return input;
}

//Overloading adding, subtraction, division, multiply and comparison operators
Money Money::operator+(const Money &pMoney)
{
	Money *tempMoney = new Money(pMoney.euros + this->euros, pMoney.centimes + this->centimes);

	return checkMoney(tempMoney);
}

Money Money::operator-(const Money &pMoney)
{
	Money *tempMoney = new Money(this->euros - pMoney.euros, this->centimes - pMoney.centimes);

	return checkMoney(tempMoney);
}

Money Money::operator*(const Money &pMoney)
{
	float result = (float(this->euros) + float(this->centimes) / 100) * (float(pMoney.euros) + float(pMoney.centimes) / 100);
	Money *tempMoney = new Money(int(result), ((result - (int)result) * 100) + 0.1);

	return checkMoney(tempMoney);
}

Money Money::operator/(const Money &pMoney)
{
	float result = (float(this->euros) + float(this->centimes) / 100) / (float(pMoney.euros) + float(pMoney.centimes) / 100);
	Money *tempMoney = new Money(int(result), ((result - (int)result) * 100) + 0.1);

	return checkMoney(tempMoney);
}

//In case you want to divide a sum of money to certain number
Money Money::operator/(int pTimes)
{
	Money *tempMoney = new Money(this->euros / pTimes, this->centimes / pTimes);

	return checkMoney(tempMoney);
}

bool Money::operator<(const Money &pMoney)
{
	if (this->euros < pMoney.euros)
	{
		return true;
	}
	else if (this->euros > pMoney.euros)
	{
		return false;
	}
	else
	{
		if (this->centimes < pMoney.centimes)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

bool Money::operator>(const Money &pMoney)
{
	if (this->euros > pMoney.euros)
	{
		return true;
	}
	else if (this->euros < pMoney.euros)
	{
		return false;
	}
	else
	{
		if (this->centimes > pMoney.centimes)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

bool Money::operator==(const Money &pMoney)
{
	if (this->euros == pMoney.euros && this->centimes == pMoney.centimes)
	{
		return true;
	}

	return false;
}

bool Money::operator!=(const Money &pMoney)
{
	if (this->euros != pMoney.euros && this->centimes != pMoney.centimes)
	{
		return true;
	}

	return false;
}